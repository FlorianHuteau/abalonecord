import { cloneDeep } from "lodash";
import { useEffect, useState } from "react";
import { Board } from "../model/Board";
import { Directions, PhaseMoving, NodeState } from "../model/enums";
import { Player } from "../model/Player";
import { Node } from "../model/Node";
import { SelectedMarbles } from "../model/SelectedMarbles";
import useWSClient from "./useWSClient";

export default function useMove(
  board: Board,
  setBoard: (board: Board) => void,
  currentPlayer: Player,
  setCurrentPlayer: (currentPlayer: Player) => void
) {
  const [marblesSelected, setMarblesSelected] = useState<SelectedMarbles>(
    new SelectedMarbles()
  );
  const [directionsEligibleToMove, setDirectionsEligibleToMove] = useState<
    Directions[]
  >([]);
  const [phaseMoving, setPhaseMoving] = useState<PhaseMoving>(
    PhaseMoving.SELECT
  );

  const [
    sendRoomJoin,
    sendRoomLeave,
    sendRoomCreate,
    sendGameStart,
    sendGameOver,
    sendForfeitGame,
    sendGameState,
    sendJoinGame,
    sendGameMovement,
  ] = useWSClient();

  useEffect(() => {
    function removeLastMarbleSelected(ev: MouseEvent) {
      ev.preventDefault();
      if (marblesSelected.length > 0) {
        setMarblesSelected(marblesSelected.slice(0, -1) as SelectedMarbles);
      }
    }
    window.addEventListener("contextmenu", removeLastMarbleSelected);

    directionsAvailablesToMove();
    return () => {
      window.removeEventListener("contextmenu", removeLastMarbleSelected);
    };
  }, [marblesSelected]);

  function handleNode(node: Node) {
    if (isSelectable(node)) {
      setMarblesSelected(marblesSelected.addMarble(node));
    } else if (isLandable(node)) {
      const directionLandingNode = directionToMove(node);
      if (
        directionLandingNode !== undefined &&
        directionsEligibleToMove.includes(directionLandingNode)
      ) {
        setPhaseMoving(PhaseMoving.MOVE);
        move(node, directionLandingNode);
        resetPhase();
        changePlayer();
      }
    }
  }

  return [marblesSelected, directionsEligibleToMove, handleNode] as const;

  function isSelectable(node: Node) {
    const selectable =
      phaseMoving === PhaseMoving.SELECT &&
      node.state === currentPlayer.color &&
      marblesSelected.length <= 3 &&
      !marblesSelected.includes(node);

    return selectable && validNode(node);
  }

  function isLandable(node: Node) {
    return (
      !marblesSelected.includes(node) &&
      marblesSelected.hasNeighbor(node) &&
      node.state !== NodeState.EDGE
    );
  }

  function validNode(node: Node) {
    switch (marblesSelected.length) {
      case 0:
        return node.hasEmptyNeighbour();
      case 1:
        return marblesSelected.hasNeighbour(node);
      case 2:
        for (const marble of marblesSelected) {
          if (
            marble.hasNeighbours(node) &&
            marblesSelected
              .orientation()
              .includes(marble.getDirectionToNode(node))
          )
            return true;
        }
    }

    return false;
  }

  function directionToMove(nodeLanding: Node) {
    if (
      marblesSelected.length === 1 &&
      marblesSelected.hasNeighbour(nodeLanding)
    ) {
      return marblesSelected[0].getDirectionToNode(nodeLanding);
    }
    const directionsToLandingNode = directionsLandingNode(nodeLanding);
    directionsAvailablesToMove();
    return directionsEligibleToMove.find((direction) =>
      directionsToLandingNode.includes(direction)
    );
  }

  function directionsLandingNode(nodeLanding: Node) {
    const directions: Directions[] = [];
    for (const marble of marblesSelected) {
      const directionToNode = marble.getDirectionToNode(nodeLanding);
      if (directionToNode !== undefined) directions.push(directionToNode);
    }

    return directions;
  }

  function move(nodeLanding: Node, directionNodeLanding: Directions) {
    const newBoard = cloneDeep(board);
    let movingMarbles = marblesSelected.reorderMarbles();

    if (nodeLanding.state !== NodeState.EMPTY) {
      movingMarbles = movingMarbles.addPushedMarbles(
        nodeLanding,
        directionNodeLanding
      );
    }

    newBoard.moveNodes(movingMarbles, directionNodeLanding);
    const gameMovement = movementSerialization(
      movingMarbles,
      directionNodeLanding
    );
    sendGameMovement(gameMovement);
    setBoard(newBoard);
  }

  function movementSerialization(
    movingMarbles: Node[],
    directionLandingNode: Directions
  ) {
    const posFirstNode =
      movingMarbles[0].position < 10
        ? "0" + movingMarbles[0].position
        : movingMarbles[0].position;
    const direction = Directions[directionLandingNode];
    if (movingMarbles.length === 1) {
      return posFirstNode + "00" + direction;
    }

    const nbMarbles = "0" + (movingMarbles.length - 1);
    const directionSelection = movingMarbles[0].getDirectionToNode(
      movingMarbles[1]
    );

    if (directionSelection === undefined) {
      throw new Error("Direction selection is undefined");
    }

    return (
      posFirstNode + nbMarbles + Directions[directionSelection] + direction
    );
  }

  function mouvementDeserialization(mouvement: string) {
    const regex = /(\w\w|[EW])/g;
    const [posFirstNode, nbMarbles, directionSelection, direction] =
      mouvement.match(regex);
    console.log(posFirstNode, nbMarbles, directionSelection, direction);

    if (posFirstNode === null || nbMarbles === null || direction === null) {
      throw new Error("Invalid mouvement");
    }

    makeMovement(posFirstNode, nbMarbles, directionSelection, direction);
  }

  function makeMovement(
    posFirstNode: string,
    nbMarbles: string,
    directionSelection: string,
    direction: string
  ) {
    const firstNode = board.board[+posFirstNode];
    const directionNodeLanding = Directions[direction];
    const movingMarbles = new SelectedMarbles(firstNode);

    if (nbMarbles !== "00") {
      for (let i = 1; i < +nbMarbles; i++) {
        const node = movingMarbles[i - 1].getNeighbor(
          Directions[directionSelection]
        );
        movingMarbles.push(node);
      }
    }

    const newBoard = cloneDeep(board);

    newBoard.moveNodes(movingMarbles, directionNodeLanding);

    setBoard(newBoard);
  }

  function resetPhase() {
    setMarblesSelected(new SelectedMarbles());
    setPhaseMoving(PhaseMoving.SELECT);
  }

  function changePlayer() {
    const newPlayer = board.getPlayerByIndex(
      currentPlayer.numero === 0 ? 1 : 0
    );
    setCurrentPlayer(newPlayer!);
  }

  function directionsAvailablesToMove() {
    if (marblesSelected.length === 0) {
      setDirectionsEligibleToMove([]);
      return;
    }

    if (marblesSelected.length === 1) {
      setDirectionsEligibleToMove(
        marblesSelected[0].getDirectionToEmptyNodes()
      );
      return;
    }

    const directionsAvailables: Directions[] = [];

    for (const marble of marblesSelected) {
      directionsAvailables.push(
        ...marble
          .getDirectionToEmptyNodes()
          .concat(getDirectionToOpponentThatCanBePushed(marble))
      );
    }

    sortDirectionsAvailables(directionsAvailables);
  }

  function getDirectionToOpponentThatCanBePushed(marble: Node) {
    return marble.getDirectionToOtherPLayer().filter((direction) => {
      const opponent = marble.getNeighbour(direction);
      return (
        marblesSelected.orientation().includes(direction) &&
        opponent !== undefined &&
        canBePushed(opponent, direction)
      );
    });
  }

  function canBePushed(node: Node, direction: Directions) {
    let emptyNode = false;
    let behindNode: Node | undefined = node.getNeighbour(direction);
    for (
      let i = 1;
      i < marblesSelected.length && !emptyNode && behindNode !== undefined;
      i++
    ) {
      emptyNode =
        emptyNode ||
        [NodeState.EMPTY, NodeState.EDGE].includes(behindNode.state);

      behindNode = behindNode.getNeighbour(direction);
    }
    return emptyNode;
  }

  function sortDirectionsAvailables(directionsAvailables: Directions[]) {
    const counterDirectionsAvailables: Record<Directions, number> = {
      [Directions.NE]: 0,
      [Directions.NW]: 0,
      [Directions.SE]: 0,
      [Directions.SW]: 0,
      [Directions.E]: 0,
      [Directions.W]: 0,
    };
    for (const direction of directionsAvailables) {
      counterDirectionsAvailables[direction]++;
    }
    findDirectionsEligiblesToMove(counterDirectionsAvailables);
  }

  function findDirectionsEligiblesToMove(
    counterDirectionsAvailables: Record<Directions, number>
  ) {
    const directionsEligibles = Object.entries(counterDirectionsAvailables)
      .filter(
        ([direction, value]) =>
          value === marblesSelected.length ||
          (value > 0 &&
            marblesSelected.orientation().includes(+direction as Directions))
      )
      .map(([direction]) => +direction as Directions);

    setDirectionsEligibleToMove(directionsEligibles);
  }
}
