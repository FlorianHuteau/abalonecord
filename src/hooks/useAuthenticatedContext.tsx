import React from 'react';
import { discordSDK } from '../utils/DiscordWrapper';
import { DiscordAuth, GuildMember } from '../types';
import { LoadingScreen } from '../components/LoadingScreen';

const AuthContext = React.createContext<DiscordAuth>({
  access_token: '',
  user: {
    username: '',
    discriminator: '',
    id: '',
    public_flags: 0,
    avatar: null,
    global_name: null
  },
  scopes: [],
  expires: '',
  application: {
    id: '',
    description: '',
    name: '',
    icon: undefined,
    rpc_origins: undefined
  },
  guildMember: null
}
);

export function AuthenticatedContextProvider({ children }: { children: React.ReactNode }) {
  const authenticatedContext = useAuthenticatedContextSetup();

  if (authenticatedContext == null) {
    return <LoadingScreen />;
  }

  return <AuthContext.Provider value={authenticatedContext}>{children}</AuthContext.Provider>;
}

export function useAuthenticatedContext() {
  return React.useContext(AuthContext);
}

function useAuthenticatedContextSetup() {
  const [auth, setAuth] = React.useState<DiscordAuth | null>(null);
  const settingUp = React.useRef(false);

  React.useEffect(() => {
    const setUpdiscordSDK = async () => {
      await discordSDK.ready();

      const { code } = await discordSDK.commands.authorize({
        client_id: import.meta.env.VITE_DISCORD_CLIENT_ID,
        response_type: 'code',
        state: '',
        prompt: 'none',
        scope: [
          // "applications.builds.upload",
          // "applications.builds.read",
          // "applications.store.update",
          // "applications.entitlements",
          // "bot",
          'identify',
          // "connections",
          // "email",
          // "gdm.join",
          'guilds',
          // "guilds.join",
          'guilds.members.read',
          // "messages.read",
          // "relationships.read",
          // 'rpc.activities.write',
          // "rpc.notifications.read",
          // "rpc.voice.write",
          'rpc.voice.read',
          // "webhook.incoming",
        ]
      });
      
      const response = await fetch('/api/api/token', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          code,
        }),
      });
      const json = await response.json();
      const { access_token } = json;

      // Authenticate with Discord client (using the access_token)
      const newAuth = await discordSDK.commands.authenticate({
        access_token,
      });
      localStorage.setItem('discordAuth', JSON.stringify(newAuth));
      localStorage.setItem('discordAuthTime', new Date().toISOString());
      localStorage.setItem('discordAuthExpires', newAuth.expires);
      // Get guild specific nickname and avatar, and fallback to user name and avatar
      const guildMember: GuildMember = await fetch(
        `https://discord.com/api/users/@me/guilds/${discordSDK.guildId}/member`,
        {
          method: 'get',
          headers: { Authorization: `Bearer ${access_token}` },
        }
      ).then((j) => j.json())
        .catch(() => {
          return null;
        });

       setAuth({ ...newAuth, guildMember });
    }
    if (!settingUp.current) {
      settingUp.current = true;
      setUpdiscordSDK();
    }
  }, []);

  return auth;
}