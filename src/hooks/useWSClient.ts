import { MutableRefObject, useEffect, useRef, useState } from "react";
import { Message, MessageType } from "../types";
import { discordSDK } from "../utils/DiscordWrapper";
import { useAuthenticatedContext } from "./useAuthenticatedContext";

export default function useWSClient() {
  const auth = useAuthenticatedContext();
  const [isReady, setIsReady] = useState(false);
  const [val, setVal] = useState("");

  const WSClient: MutableRefObject<WebSocket | null> = useRef(null);
  const userID = auth.user.id;

  useEffect(() => {
    const socket = new WebSocket(
      `wss://${discordSDK.clientId}.discordsays.com/api/ws?access_token=${auth.access_token}`
    );
    socket.onopen = () => setIsReady(true);
    socket.onclose = () => setIsReady(false);
    socket.onmessage = (event) => {
      console.log("onmessage", event.data);

      setVal(JSON.parse(event.data));
    };

    WSClient.current = socket;

    return () => {
      socket.close();
    };
  }, []);

  useEffect(() => {
    if (WSClient.current && val) {
      switch (val.type) {
        case MessageType.GAME_MOVEMENT:
          handleGameMovement();
          break;
        case MessageType.FORFEIT_GAME:
          handleForfeitGame();
          break;
        case MessageType.GAME_OVER:
          handleGameOver();
          break;
        case MessageType.GAME_START:
          handleGameStart();
          break;
        case MessageType.ROOM_UPDATE:
          handleRoomUpdate();
          break;
        default:
          console.log("Unknown message type", val);
      }
    }
  }, [val]);

  function sendGameMovement(gameMovement: string) {
    const data: Message = {
      type: MessageType.GAME_MOVEMENT,
      content: {
        player_id: userID,
        movement: gameMovement,
      },
    };
    console.log("Sending game movement", data);

    WSClient.current?.send(JSON.stringify(data));
  }

  function handleGameMovement() {
    console.log("Game movement received", val);
  }

  function sendJoinGame() {
    const data: Message = {
      type: MessageType.JOIN_GAME,
      content: {
        player_id: userID,
        room_code: "123456",
      },
    };
    WSClient.current?.send(JSON.stringify(data));
  }

  function sendGameState() {
    const data: Message = {
      type: MessageType.GAME_STATE,
      content: {},
    };
    if (WSClient.current?.readyState === WebSocket.OPEN) {
      WSClient.current?.send(JSON.stringify(data));
    }
  }

  function sendForfeitGame() {
    const data: Message = {
      type: MessageType.FORFEIT_GAME,
      content: {},
    };
    WSClient.current?.send(JSON.stringify(data));
  }

  function handleForfeitGame() {
    console.log("Forfeit game received", val);
  }

  function sendGameOver() {
    const data: Message = {
      type: MessageType.GAME_OVER,
      content: {
        winner_id: "123",
      },
    };
    WSClient.current?.send(JSON.stringify(data));
  }

  function handleGameOver() {
    console.log("Game over received", val);
  }

  function sendGameStart() {
    const data: Message = {
      type: MessageType.GAME_START,
      content: {
        black_player_id: "123",
        white_player_id: "456",
      },
    };
    WSClient.current?.send(JSON.stringify(data));
  }

  function handleGameStart() {
    console.log("Game start received", val);
  }

  function sendRoomJoin() {
    const data: Message = {
      type: MessageType.ROOM_JOIN,
      content: {
        room_code: "123456",
        player_id: "123",
      },
    };
    WSClient.current?.send(JSON.stringify(data));
  }

  function sendRoomLeave() {
    const data: Message = {
      type: MessageType.ROOM_LEAVE,
      content: {
        room_code: "123456",
        player_id: "123",
      },
    };
    WSClient.current?.send(JSON.stringify(data));
  }

  function sendRoomCreate() {
    const data: Message = {
      type: MessageType.ROOM_CREATE,
      content: {
        room_code: "123456",
        player_id: "123",
        public: true,
      },
    };
    WSClient.current?.send(JSON.stringify(data));
  }

  function handleRoomUpdate() {
    console.log("Room update received", val);
  }

  return [
    sendRoomJoin,
    sendRoomLeave,
    sendRoomCreate,
    sendGameStart,
    sendGameOver,
    sendForfeitGame,
    sendGameState,
    sendJoinGame,
    sendGameMovement,
  ] as const;
}
