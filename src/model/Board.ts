import { Directions, NodeState } from "./enums";
import { Node } from "./Node";
import { Player } from "./Player";

export class Board {
  localPlayer: Player;
  otherPlayers: Player[];
  board: Node[] = [];
  cellByRow: number[] = [5, 6, 7, 8, 9, 8, 7, 6, 5];
  scores: number[];

  constructor(localPlayer: Player, otherPlayers: Player[]) {
    this.localPlayer = localPlayer;
    this.otherPlayers = otherPlayers;
    this.scores = [0, 0];
    this.initBoard();
  }

  public initBoard() {
    /**
     *
     * @param board
     * @param index
     * @param lineSize
     * @param decreasing if the board in increasing or decreasing in line size
     * @returns
     */
    function createInnerNodes(
      board: Node[],
      index: number,
      lineSize: number,
      decreasing: boolean
    ) {
      for (let y = 0; y < lineSize; y++) {
        const current = createNodeInBoard(index, NodeState.EMPTY, board);
        connectNodeToPreviousRow(current, board, index, lineSize, decreasing);
        current.connect(Directions.W, board[index - 1]);
        index++;
      }

      return index;
    }

    function connectNodeToPreviousRow(
      node: Node,
      board: Node[],
      index: number,
      lineSize: number,
      decreasing: boolean
    ) {
      const neIndex = calculNE(index, lineSize, decreasing);
      const nwIndex = calculNW(index, lineSize, decreasing);
      node.connect(Directions.NW, board[nwIndex]);
      node.connect(Directions.NE, board[neIndex]);
    }

    function createTopEdge(sizeFirstRow: number, board: Node[]) {
      let index = 0;
      for (index; index <= sizeFirstRow; index++) {
        createNodeInBoard(index, NodeState.EDGE, board);
      }
      return index;
    }

    function createBottomEdge(
      index: number,
      sizeLastRow: number,
      board: Node[]
    ) {
      for (let i = 0; i <= sizeLastRow; i++) {
        const current = createNodeInBoard(index, NodeState.EDGE, board);
        connectNodeToPreviousRow(current, board, index, sizeLastRow, true);
        index++;
      }
    }

    function calculNW(index: number, lineSize: number, decreasing: boolean) {
      return decreasing ? index - lineSize - 3 : index - lineSize - 2;
    }

    function calculNE(index: number, lineSize: number, decreasing: boolean) {
      return decreasing ? index - lineSize - 2 : index - lineSize - 1;
    }

    function createNodeInBoard(index: number, state: NodeState, board: Node[]) {
      const node = new Node(index, state);
      board.push(node);
      return node;
    }

    function createRow(index: number, nbCellByRow: number[], board: Node[]) {
      nbCellByRow.forEach(
        (sizeRow: number, indexRow: number, arrayRow: number[]) => {
          const decreasing = arrayRow[indexRow - 1] > sizeRow;
          createEdgeWest(index++, sizeRow, decreasing, board);
          index = createInnerNodes(board, index++, sizeRow, decreasing);
          createEdgeEast(index, sizeRow, decreasing, board);
          index++;
        }
      );

      return index++;
    }

    function createEdgeWest(
      index: number,
      sizeRow: number,
      decreasing: boolean,
      board: Node[]
    ) {
      const edgeWest = createNodeInBoard(index, NodeState.EDGE, board);
      connectNodeToPreviousRow(edgeWest, board, index, sizeRow, decreasing);
    }

    function createEdgeEast(
      index: number,
      sizeRow: number,
      decreasing: boolean,
      board: Node[]
    ) {
      const edgeEast = createNodeInBoard(index, NodeState.EDGE, board);
      connectNodeToPreviousRow(edgeEast, board, index, sizeRow, decreasing);
      edgeEast.connect(Directions.W, board[index - 1]);
    }

    let indexNode = createTopEdge(this.cellByRow[0], this.board);
    indexNode = createRow(indexNode, this.cellByRow, this.board);
    createBottomEdge(
      indexNode,
      this.cellByRow[this.cellByRow.length - 1],
      this.board
    );
  }

  public mixBoardAndCellByRow() {
    const cpBoard = this.board.slice();
    const mix: Node[][] = [];

    mix.push(cpBoard.splice(0, this.cellByRow[0] + 1));
    this.cellByRow.forEach((size) => {
      const row = cpBoard.splice(0, size + 2);
      mix.push(row);
    });
    mix.push(cpBoard);

    return mix;
  }

  public initMatch1v1() {
    const whitePart = [7, 8, 9, 10, 11, 14, 15, 16, 17, 18, 19, 24, 25, 26];
    const blackPart = [64, 65, 66, 71, 72, 73, 74, 75, 76, 79, 80, 81, 82, 83];

    this.board.forEach((node, index) => {
      if (whitePart.includes(index)) {
        node.state = NodeState.WHITE;
      } else if (blackPart.includes(index)) {
        node.state = NodeState.BLACK;
      }
    });
  }

  public moveNodes(nodesMoving: Node[], directionLandingNodes: Directions) {
    const newCells: Node[] = [];
    for (const node of nodesMoving) {
      const exCell = this.board[node.position];
      const newCell = exCell.getNeighbour(directionLandingNodes);

      if (newCell !== undefined) {
        newCells.push(newCell);
        if (newCell.state === NodeState.EDGE) {
          this.ejection(nodesMoving[nodesMoving.length - 1].state);
        } else newCell.state = exCell.state;
        if (!newCells.includes(exCell)) exCell.state = NodeState.EMPTY;
      }
    }
  }

  public ejection(color: NodeState) {
    const ejector = this.getPlayerByColor(color);
    if (ejector !== undefined) {
      ejector.score++;
      if (ejector.score === 6) console.log("You win");
    }
  }

  public getPlayers() {
    return [this.localPlayer, ...this.otherPlayers];
  }

  public getPlayerByIndex(index: number) {
    return [this.localPlayer, ...this.otherPlayers].find(
      (player) => player.numero === index
    );
  }

  public getPlayerByColor(color: NodeState) {
    return [this.localPlayer, ...this.otherPlayers].find(
      (player) => player.color === color
    );
  }

  public otherPLayersColors(node: Node) {
    for (const player of this.otherPlayers) {
      if (node.state === player.color) {
        return true;
      }

      return false;
    }
  }
}
