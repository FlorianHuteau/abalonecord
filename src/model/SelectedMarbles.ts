import { Directions, NodeState } from "./enums";
import { Node } from "./Node";

interface ISelectedMarbles {
  orientation(): (Directions | undefined)[];
  addPushedMarbles(node: Node, direction: Directions): Node[];
  reorderMarbles(): Node[];
  hasNeighbour(node: Node): boolean;
}

export class SelectedMarbles extends Array<Node> implements ISelectedMarbles {
  constructor(...marbles: Node[]) {
    super(...marbles);
    Object.setPrototypeOf(this, Object.create(SelectedMarbles.prototype));
  }

  orientation() {
    if (this.length < 2) throw new Error("Not enough marbles to move");
    return [
      this[0].getDirectionToNode(this[1]),
      this[1].getDirectionToNode(this[0]),
    ];
  }

  addMarble(node: Node) {
    if (this.length > 3) throw new Error("Too many marbles selected");
    if (this.includes(node)) throw new Error("Marble already selected");

    return new SelectedMarbles(...this, node);
  }

  addPushedMarbles(node: Node, direction: Directions) {
    let marblesPushed = [node];
    const nodeBehind = node.getNeighbour(direction);
    if (
      nodeBehind !== undefined &&
      ![NodeState.EMPTY, NodeState.EDGE].includes(nodeBehind.state)
    ) {
      marblesPushed = [nodeBehind, node];
    }
    return [...marblesPushed, ...this] as SelectedMarbles;
  }

  reorderMarbles() {
    if (this.length < 3) {
      return this;
    }

    return this.sort((a: Node, b: Node) => {
      return a.position - b.position;
    });
  }

  hasNeighbour(node: Node) {
    return this.some((marble: Node) => marble.hasNeighbours(node));
  }
}
