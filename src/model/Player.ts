import { NodeState } from "./enums";

export class Player {
  playerID: string;
  color: NodeState;
  numero: number;
  score: number = 0;
  constructor(color: NodeState, numero: number) {
    this.color = color;
    this.numero = numero;
  }
}
