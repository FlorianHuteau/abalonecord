export enum Directions {
  NE,
  NW,
  SE,
  SW,
  E,
  W,
}

export function getOppositeDirection(direction: Directions) {
  switch (direction) {
    case Directions.NE:
      return Directions.SW;
    case Directions.NW:
      return Directions.SE;
    case Directions.SE:
      return Directions.NW;
    case Directions.SW:
      return Directions.NE;
    case Directions.E:
      return Directions.W;
    case Directions.W:
      return Directions.E;
  }
}

export function getArrowRotation(direction: Directions) {
  switch (direction) {
    case Directions.NE:
      return "NE_orientation";
    case Directions.NW:
      return "NW_orientation";
    case Directions.SE:
      return "SE_orientation";
    case Directions.SW:
      return "SW_orientation";
    case Directions.E:
      return "E_orientation";
    case Directions.W:
      return "W_orientation";
  }
}

export enum NodeState {
  BLACK,
  WHITE,
  YELLOW,
  RED,
  EMPTY,
  EDGE,
}

export function displayState(state: NodeState) {
  switch (state) {
    case NodeState.BLACK:
      return "black-marble";
    case NodeState.WHITE:
      return "white-marble";
    case NodeState.YELLOW:
      return "yellow-marble";
    case NodeState.RED:
      return "red-marble";
    case NodeState.EMPTY:
      return "empty-marble";
    case NodeState.EDGE:
      return "empty-marble";
  }
}

export enum PhaseMoving {
  SELECT,
  MOVE,
  LAND,
}
