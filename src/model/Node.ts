import {
  Directions,
  NodeState,
  displayState,
  getOppositeDirection,
} from "./enums";

export class Node {
  private _neighbours: Record<Directions, Node | undefined> = {
    [Directions.NE]: undefined,
    [Directions.NW]: undefined,
    [Directions.SE]: undefined,
    [Directions.SW]: undefined,
    [Directions.E]: undefined,
    [Directions.W]: undefined,
  };
  public get state() {
    return this._state;
  }

  public set state(value: NodeState) {
    this._state = value;
  }

  public getNeighbour(direction: Directions) {
    return this._neighbours[direction];
  }

  public setNeighbour(direction: Directions, node: Node | undefined) {
    this._neighbours[direction] = node;
  }

  public hasNeighbours(node: Node) {
    return Object.values(this._neighbours).includes(node);
  }

  public hasEmptyNeighbour() {
    return Object.values(this._neighbours).some(
      (neighbour) => neighbour?.state === NodeState.EMPTY
    );
  }

  constructor(
    public readonly position: number, // The position of the node in the list, should be communicated to the server if targeted for a token movement
    private _state: NodeState
  ) {}

  public connect(direction: Directions, node: Node) {
    this._neighbours[direction] = node;

    if (node !== undefined) {
      node.setNeighbour(getOppositeDirection(direction), this);
    }
  }

  public toString() {
    return displayState(this._state);
  }

  getDirectionToNode(node: Node) {
    const direction = Object.entries(this._neighbours).find(
      ([, neighbour]) => neighbour === node
    )?.[0];

    return direction !== undefined ? (+direction as Directions) : undefined;
  }

  getOppositeDirectionToNode(node: Node) {
    const direction = this.getDirectionToNode(node);

    return direction !== undefined
      ? getOppositeDirection(direction)
      : undefined;
  }

  getDirectionToEmptyNodes() {
    const emptyNodes = Object.entries(this._neighbours).filter(
      ([, neighbour]) => neighbour?.state === NodeState.EMPTY
    );
    return emptyNodes.map(([direction]) => +direction as Directions);
  }

  getDirectionToOtherPLayer() {
    const otherPlayerState = [
      NodeState.BLACK,
      NodeState.WHITE,
      NodeState.YELLOW,
      NodeState.RED,
    ].filter((state) => state !== this._state);

    const otherPlayerNodes = Object.entries(this._neighbours).filter(
      ([, neighbour]) => {
        if (neighbour === undefined) {
          return false;
        }
        return otherPlayerState.includes(neighbour.state);
      }
    );
    return otherPlayerNodes.map(([direction]) => +direction as Directions);
  }
}
