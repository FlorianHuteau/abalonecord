import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { DiscordUser } from "../types";

interface Player {
  id: string;
  rank: number;
  name: string;
  elo: number;
}

/**
 * `RankingScreen` component.
 * 
 * This component is responsible for rendering a ranking table of players, displaying their ranks,
 * names, and scores. It also includes a return button to navigate back to the home page.
 * The player data is fetched from a local JSON file, and additional user details (like Discord username 
 * and profile picture) are fetched from the Discord API using the player IDs.
 * 
 * @component
 */
export function RankingScreen() {
  const navigate = useNavigate(); // Hook for programmatic navigation
  const [players, setPlayers] = useState<Player[]>([]); // State to store player data
  const [userDetails, setUserDetails] = useState<Record<string, DiscordUser>>({}); // State to store Discord user details by player ID

  // Fetch player data from a local JSON file on component mount
  useEffect(() => {
    fetch("../../public/data/dummyData.json")
      .then(response => response.json())
      .then(data => {
        setPlayers(data.players);

        // Fetch additional user details for each player
        data.players.forEach((player: Player) => {
          fetchUserDetails(player.id);
        });
      });
  }, []);

  /**
   * Fetch Discord user details by user ID.
   * 
   * param {string} userId - The ID of the Discord user.
   */
  const fetchUserDetails = async (userId: string) => {
    try {
      const response = await fetch(`/user/${userId}`);
      if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
      }
      const user: DiscordUser = await response.json();
      // Store the fetched user details in the state
      setUserDetails(prevDetails => ({ ...prevDetails, [userId]: user }));
    } catch (error) {
      console.error(`Failed to fetch user details for ${userId}`, error);
    }
  };

  /**
   * Handle return button click to navigate back to the home page.
   */
  const handleReturnClick = () => {
    navigate("/");
  };

  return (
    <>
      <div className="return-button-wrapper" onClick={handleReturnClick}>
        <button type="button" title="Return" className="icon-button">
          <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 256 256">
            <path
              d="M224,128a8,8,0,0,1-8,8H59.31l58.35,58.34a8,8,0,0,1-11.32,11.32l-72-72a8,8,0,0,1,0-11.32l72-72a8,8,0,0,1,11.32,11.32L59.31,120H216A8,8,0,0,1,224,128Z"
            >
            </path>
          </svg>
        </button>
      </div>
      <div className="main-container">
        <div className="container overflow-container">
          <h1 className="abalone-title">Ranks</h1>
          <div className="content-wrapper">
            <table className="ranking-table">
              <thead>
                <tr>
                  <th>Rank</th>
                  <th>Name</th>
                  <th>Score</th>
                </tr>
              </thead>
              <tbody>
                {players.map((player) => {
                  const user = userDetails[player.id];
                  return (
                    <tr key={player.rank}>
                      <td>{player.rank}</td>
                      <td>
                        {user ? (
                          <div>
                            <img 
                              src={`https://cdn.discordapp.com/avatars/${user.id}/${user.avatar}.webp`} 
                              alt={user.username} 
                              style={{ width: '32px', height: '32px', borderRadius: '50%', marginRight: '8px' }} 
                            />
                            {user.username}
                          </div>
                        ) : (
                          "Loading..."
                        )}
                      </td>
                      <td>{player.elo}</td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </>
  );
}
