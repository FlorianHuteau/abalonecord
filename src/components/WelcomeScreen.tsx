import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import useWSClient from "../hooks/useWSClient";

export function WelcomeScreen() {
  const [joiningRoom, setJoiningRoom] = useState(false);
  const [roomCode, setRoomCode] = useState("");
  const navigate = useNavigate();
  // const [sendGameState] = useWSClient();

  const handleJoinRoomClick = () => {
    setJoiningRoom(!joiningRoom);
  };

  const handleCreateRoomClick = () => {
    navigate("/game"); // Redirect vers /game
  };

  const handleRankingClick = () => {
    navigate("/ranking"); // Redirige vers /ranking
  };

  const handleRoomCodeChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const code = event.target.value.replace(/\D/g, ""); // Remove all non-digit characters
    if (code.length <= 6) {
      setRoomCode(code);
    }
  };

  return (
    <>
      
      <div className="container">
        <h1 className="abalone-title">Welcome to Abalone</h1>
        {joiningRoom ? (
          <>
            <div className="container">
              <label htmlFor="room-code">Room Code</label>
              <input
                id="room-code"
                type="text"
                value={roomCode}
                onChange={handleRoomCodeChange}
                maxLength={6}
                pattern="\d{6}"
                title="Room code should be 6 digits"
              />
            </div>
            <div className='rooms-btn-container'>
              <button className="rooms-btn" onClick={handleJoinRoomClick}>Back</button>
              <button className="rooms-btn" onClick={handleCreateRoomClick}>Create Room</button>
            </div>
          </>
        ) : (

        
        <div className='btn-container'>
            <button role="button" onClick={handleCreateRoomClick}>Create Room</button>
            <button role="button" onClick={handleJoinRoomClick}>
              Join Room
            </button>
          </div>
        )}
        <button className="ranking-button" onClick={handleRankingClick}>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="32"
            height="32"
            viewBox="0 0 256 256"
          >
            <path d="M232,64H208V48a8,8,0,0,0-8-8H56a8,8,0,0,0-8,8V64H24A16,16,0,0,0,8,80V96a40,40,0,0,0,40,40h3.65A80.13,80.13,0,0,0,120,191.61V216H96a8,8,0,0,0,0,16h64a8,8,0,0,0,0-16H136V191.58c31.94-3.23,58.44-25.64,68.08-55.58H208a40,40,0,0,0,40-40V80A16,16,0,0,0,232,64ZM48,120A24,24,0,0,1,24,96V80H48v32q0,4,.39,8Zm144-8.9c0,35.52-29,64.64-64,64.9a64,64,0,0,1-64-64V56H192ZM232,96a24,24,0,0,1-24,24h-.5a81.81,81.81,0,0,0,.5-8.9V80h24Z"></path>
          </svg>
        </button>
      </div>
    </>
  );
}
