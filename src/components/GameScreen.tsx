import { useState } from "react";
import { NodeState } from "../model/enums";
import { Player } from "../model/Player";
import { Board } from "../model/Board";
import { useAuthenticatedContext } from "../hooks/useAuthenticatedContext";
import { PageBoard } from "../view/PageBoard";
import { useNavigate } from "react-router-dom";



export function GameScreen() {
  const navigate = useNavigate();
  const auth = useAuthenticatedContext();
  const [currentPlayer, setCurrentPlayer] = useState<Player>();
  const [board, setBoard] = useState<Board>(
    new Board(new Player(NodeState.BLACK, 0), [new Player(NodeState.WHITE, 1)])
  );

  const handleReturnClick = () => {
    navigate("/");
  };

  return (
    <>
    <div className="return-button-wrapper" onClick={handleReturnClick}>
        <button type="button" title="Return" className="icon-button">
          <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 256 256">
            <path
              d="M224,128a8,8,0,0,1-8,8H59.31l58.35,58.34a8,8,0,0,1-11.32,11.32l-72-72a8,8,0,0,1,0-11.32l72-72a8,8,0,0,1,11.32,11.32L59.31,120H216A8,8,0,0,1,224,128Z"
            >
            </path>
          </svg>
        </button>
      </div>
    <div className="main_container">
      {/* <p>{auth?.user.username}</p> */}
      <div className="game_container">
        <div className="players">
          <img
            src={
              "https://cdn.discordapp.com/avatars/" +
              auth?.user.id +
              "/" +
              auth?.user.avatar +
              ".webp"
            }
            alt="avatar"
            className="avatar"
            style={
              currentPlayer?.color === NodeState.WHITE
                ? { border: "2px solid red" }
                : {}
            }
          />
          <p className="pseudo">{auth?.user.global_name}</p>
          <p>VS</p>
          <img
            src={
              "https://cdn.discordapp.com/avatars/" +
              auth?.user.id +
              "/" +
              auth?.user.avatar +
              ".webp"
            }
            alt="avatar"
            className="avatar"
            style={
              currentPlayer?.color === NodeState.BLACK
                ? { border: "2px solid red" }
                : {}
            }
          />
          <p className="pseudo">{auth?.user.global_name}</p>
        </div>
        <div className="board-wrapper">
          <svg className="board" viewBox="95 -65 400 400">
            <PageBoard
              currentPlayer={currentPlayer}
              setCurrentPlayer={setCurrentPlayer}
              board={board}
              setBoard={setBoard}
            />
          </svg>
        </div>
        <div className="game-info">
          <h1>Scores</h1>
          <p className="scores">
            {board
              .getPlayers()
              .map((player) => player.score)
              .join(" - ")}
          </p>
          <div className="room-code">
            <p>Room code :</p>
            <p>XXXXXX</p>
          </div>
        </div>
      </div>
    </div>
    </>
  );
}
