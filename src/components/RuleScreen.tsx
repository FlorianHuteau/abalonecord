import { useNavigate } from "react-router-dom";
import diagram1 from "../assets/images/diagram1.png";
import diagram2 from "../assets/images/diagram2.png";
import diagram3 from "../assets/images/diagram3.png";
import diagram4 from "../assets/images/diagram4.png";
import diagram5 from "../assets/images/diagram5.png";


/**
 * `RankingScreen` component.
 * 
 * 
 * @component
 */
export function RuleScreen() {
    const navigate = useNavigate(); // Hook for programmatic navigation


    /**
     * Handle return button click to navigate back to the home page.
     */
    const handleReturnClick = () => {
        navigate("/");
    };

    return (
        <>
        
            <div className="return-button-wrapper" onClick={handleReturnClick}>
                <button type="button" title="Return" className="icon-button">
                    <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 256 256">
                        <path
                            d="M224,128a8,8,0,0,1-8,8H59.31l58.35,58.34a8,8,0,0,1-11.32,11.32l-72-72a8,8,0,0,1,0-11.32l72-72a8,8,0,0,1,11.32,11.32L59.31,120H216A8,8,0,0,1,224,128Z"
                        >
                        </path>
                    </svg>
                </button>
            </div>
            <div className="main-container">
                <div className="container overflow-container rules-container">
                    <h1 className="abalone-title">Rules</h1>
                    <h2>GAME RULES FOR 2 PLAYERS</h2>
                    <h3>GAME PLAY</h3>
                    <p>On their turn, each player may move either a single marble or a column of marbles of their own color one space.</p>
                    <p>A column consists of two or three marbles of the same color directly adjacent to one another in a straight line.</p>
                    <p>A marble or a column can move in any direction in an in-line move or side-step move.</p>
                    <p>See <strong>Diagrams 2 & 3</strong></p>
                    <div className="diagram">
                        <img src={diagram2} alt="Diagram 2" />
                    </div>
                    <p>An “In-line” Move: Marbles are moved as a column into a free space.</p>

                    <div className="diagram">
                        <img src={diagram3} alt="Diagram 3" />
                    </div>
                    <p>A 'Side step' move: Marbles are moved sideways into adjacent free spaces. A 'Side step' move cannot be used to push an opponent's single marble or column.</p>

                    <p>All marbles in a column must move in the same direction. (Diagram 3).</p>
                    <p>Unless in a Sumito position, as described below, a marble or a column must move into a free space.</p>

                    <h3>SUMITO</h3>
                    <p>When a player's column faces a lesser number of the opponent's marbles, the player has a Sumito, or advantage.</p>
                    <p>In a Sumito position, a player's column of three marbles may push one or two of the opponent's marbles one space, or a player's column of two marbles may push one of the opponent's marbles one space. (Diagram 4).</p>
                    <div className="diagram">
                        <img src={diagram4} alt="Diagram 4" />
                    </div>
                    <p><strong>It is important to note the following :</strong></p>
                    <ul>
                        <li>When pushing, the opponent's marbles must be pushed either into an unoccupied space or off the board into the outer rim.</li>
                        <li>Marbles pushed into the outer rim are no longer in play.</li>
                        <li>A single marble can never push an opposing marble, nor can a Column push any marble if it moves sideways.</li>
                        <li>Enemy marbles sandwiched between friendly marbles may not be pushed. (Diagram 5)</li>
                        <li>At any turn, no more than 3 friendly marbles can be moved, thus an opponent's Column of three can never be pushed. A position of 4-on-3 or greater is not considered a Sumito.</li>
                    </ul>
                    <div className="diagram">
                        <img src={diagram5} alt="Diagram 5" />
                    </div>
                    <p>
                        In these examples the black cannot push the white for the following reasons:
                    </p>
                    <ol>
                        <li>The white marble is sandwiched between the black marbles. There is therefore no free space for the white to be pushed onto.</li>
                        <li>The black and the white columns are separated by a free space.</li>
                        <li>A side stepping column cannot push any marble.</li>
                    </ol>
                    <h3>GAME END</h3>
                    <p>As soon as a player has had six marbles pushed off the board into the outer rim, the game is over and the opponent wins.</p>
                </div>
            </div>
        </>
    );
}
