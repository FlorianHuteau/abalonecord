import { FC, HtmlHTMLAttributes, useState } from "react";
import { Node } from "../../model/Node";
import { Directions, getArrowRotation } from "../../model/enums";

interface NodeCellProps {
  nodes: Node[];
  rowNumber: number;
  widestRow: number;
  cellByRow: number[];
  marblesMoving: Node[];
  clickNodeHandler: (node: Node) => void;
  directionsEligibleToMove: Directions[];
}

export const NodeCell: FC<NodeCellProps> = ({
  nodes,
  rowNumber,
  widestRow,
  cellByRow,
  marblesMoving,
  clickNodeHandler,
  directionsEligibleToMove,
}) => {
  const [cursorDirection, setCursorDirection] = useState<Directions>(
    Directions.W
  );

  const calculateCursorDirection = (event: React.MouseEvent<SVGElement>) => {
    // Get the coordinates of the center of the circle

    const circle = event.currentTarget.getBoundingClientRect();
    const circleCenterX = circle.left + circle.width / 2;
    const circleCenterY = circle.top + circle.height / 2;

    // Retrieves cursor coordinates relative to circle
    const cursorX = event.clientX - circleCenterX;
    const cursorY = event.clientY - circleCenterY;

    const angle = Math.atan2(cursorY, cursorX);

    let degrees = (angle * 180) / Math.PI;

    if (degrees < 0) {
      degrees += 360;
    }

    console.log(degrees);
  
    // Calculates direction based on angle
    let direction: Directions;
    if (degrees >= 337.5 || degrees < 22.5) {
      direction = Directions.E;
    } else if (degrees >= 22.5 && degrees < 67.5) {
      direction = Directions.SE;
    } else if (degrees >= 67.5 && degrees < 112.5) {
      direction = Directions.SW;
    } else if (degrees >= 112.5 && degrees < 157.5) {
      direction = Directions.W;
    } else if (degrees >= 157.5 && degrees < 202.5) {
      direction = Directions.NW;
    } else if (degrees >= 202.5 && degrees < 247.5) {
      direction = Directions.NE;
    } else {
      direction = Directions.E;
    }

    setCursorDirection(direction);
  };

  return (
    <g id="row">
      {nodes.map((node, index) => {
        // CHeck of the actual node is selected
        const isMoving = marblesMoving.includes(node);
        let landing = false;
        for (const direction of directionsEligibleToMove) {
          for (const marble of marblesMoving) {
            if (node === marble.getNeighbor(direction)) {
              landing = true;
            }
          }
        }
        const circlePositionX =
          31 * (index + (widestRow - cellByRow[rowNumber] / 2));
        const circlePositionY = 27 * rowNumber;

        return (
          <g>
            {/* {isMoving && (
              <polygon 
              points={`${circlePositionX - 6},${circlePositionY} ${circlePositionX + 4},${circlePositionY - 4} ${circlePositionX + 4},${circlePositionY + 4}`}
              fill="red"
              className={getArrowRotation(cursorDirection)} />
            )} */}
            <circle
              key={index}
              cx={circlePositionX}
              cy={circlePositionY}
              r={10.7}
              className={node.toString()}
              onClick={() => clickNodeHandler(node)}
              stroke="red"
              stroke-width={isMoving || landing ? "1" : "0"}
              onMouseMove={calculateCursorDirection}
            />
          </g>
        );
      })}
    </g>
  );
};
