import "./App.css";
import { GameScreen } from "./components/GameScreen";
import { RankingScreen } from "./components/RankingScreen";
import { RuleScreen } from "./components/RuleScreen";
import { WelcomeScreen } from "./components/WelcomeScreen";
import { AuthenticatedContextProvider } from "./hooks/useAuthenticatedContext";
import { BrowserRouter, Route, Routes } from "react-router-dom";


function App() {
  return (
    // elements within the AuthenticatedContextProvider will have access to the user infos @see useAuthenticatedContext
    <>
      <AuthenticatedContextProvider>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<WelcomeScreen />} />
            <Route path="/game" element={<GameScreen />} />
            <Route path="/ranking" element={<RankingScreen/>}/>
            <Route path="/rule" element={<RuleScreen/>}/>
          </Routes>
        </BrowserRouter>
      </AuthenticatedContextProvider>
    </>
  );
}

export default App;
   