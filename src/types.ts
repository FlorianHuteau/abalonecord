import { AsyncReturnType } from "type-fest";
import { discordSDK } from "./utils/DiscordWrapper";

//region Discord API Types
export type DiscordUser = {
  username: string;
  discriminator: string;
  id: string;
  bot: boolean;
  avatar_decoration_data: {
    asset: string;
    sku_id?: string | undefined;
  } | null;
  global_name?: string | null | undefined;
  avatar?: string | null | undefined;
  flags?: number | null | undefined;
  premium_type?: number | null | undefined;
};

export type DiscordMember = {
  user: DiscordUser;
  roles: string[];
  joined_at: string;
  deaf: boolean;
  mute: boolean;
  nick?: string | null | undefined;
};

// channel return type from getChannel
export type DiscordChannel = {
  type: 0 | 10 | 1 | 4 | 3 | -1 | 2 | 5 | 6 | 11 | 12 | 13 | 14 | 15;
  id: string;
  voice_states: {
    user: DiscordUser;
    nick: string;
    mute: boolean;
    voice_state: {
      deaf: boolean;
      mute: boolean;
      self_mute: boolean;
      self_deaf: boolean;
      suppress: boolean;
    };
    volume: number;
  }[];
  messages: DiscordMessage[];
  guild_id?: string | null | undefined;
  name?: string | null | undefined;
  topic?: string | null | undefined;
  bitrate?: number | null | undefined;
  user_limit?: number | null | undefined;
  position?: number | null | undefined;
};

export type DiscordMessage = {
  type: number;
  id: string;
  timestamp: string;
  channel_id: string;
  content: string;
  tts: boolean;
  mention_everyone: boolean;
  mentions: DiscordUser[];
  mention_roles: string[];
  mention_channels: DiscordChannel[];
  attachments: {
    id: string;
    url: string;
    size: number;
    filename: string;
    proxy_url: string;
    height?: number | null | undefined;
    width?: number | null | undefined;
  }[];
  embeds: {
    title?: string | null | undefined;
    type?: string | null | undefined;
    description?: string | null | undefined;
    url?: string | null | undefined;
    timestamp?: string | null | undefined;
    color?: number | null | undefined;
    footer?:
      | {
          text: string;
          icon_url?: string | null | undefined;
          proxy_icon_url?: string | null | undefined;
        }
      | null
      | undefined;
    image?:
      | {
          url?: string | null | undefined;
          proxy_url?: string | null | undefined;
          height?: number | null | undefined;
          width?: number | null | undefined;
        }
      | null
      | undefined;
    thumbnail?:
      | {
          url?: string | null | undefined;
          proxy_url?: string | null | undefined;
          height?: number | null | undefined;
          width?: number | null | undefined;
        }
      | null
      | undefined;
    video?:
      | {
          url?: string | null | undefined;
          height?: number | null | undefined;
          width?: number | null | undefined;
        }
      | null
      | undefined;
    provider?:
      | {
          name?: string | null | undefined;
          url?: string | null | undefined;
        }
      | null
      | undefined;
    author?:
      | {
          name?: string | null | undefined;
          url?: string | null | undefined;
          icon_url?: string | null | undefined;
          proxy_icon_url?: string | null | undefined;
        }
      | null
      | undefined;
    fields?:
      | {
          value: string;
          name: string;
          inline: boolean;
        }[]
      | null
      | undefined;
  }[];
  pinned: boolean;
  guild_id?: string | null | undefined;
  author?: DiscordUser | null | undefined;
  member?: DiscordMember | null | undefined;
  edited_timestamp?: string | null | undefined;
  reactions?:
    | {
        emoji: {
          id: string;
          name?: string | null | undefined;
          roles?: string[] | null | undefined;
          user?: DiscordUser | null | undefined;
          require_colons?: boolean | null | undefined;
          managed?: boolean | null | undefined;
          animated?: boolean | null | undefined;
          available?: boolean | null | undefined;
        };
        count: number;
        me: boolean;
      }[]
    | null
    | undefined;
  nonce?: string | number | null | undefined;
  webhook_id?: string | null | undefined;
  activity?:
    | {
        type: number;
        party_id?: string | null | undefined;
      }
    | null
    | undefined;
  application?:
    | {
        id: string;
        description: string;
        name: string;
        cover_image?: string | null | undefined;
        icon?: string | null | undefined;
      }
    | null
    | undefined;
  message_reference?:
    | {
        message_id?: string | null | undefined;
        channel_id?: string | null | undefined;
        guild_id?: string | null | undefined;
      }
    | null
    | undefined;
  flags?: number | null | undefined;
  stickers?: unknown[] | null | undefined;
  referenced_message?: unknown;
};
export interface GuildMember {
  roles: string[];
  nick: string | null;
  avatar: string | null;
  premium_since: string | null;
  joined_at: string;
  is_pending: boolean;
  pending: boolean;
  communication_disabled_until: string | null;
  user: DiscordUser;
  mute: boolean;
  deaf: boolean;
}

export type DiscordChannelPromise = AsyncReturnType<
  typeof discordSDK.commands.getChannel
>;
export type DiscordAuth = ReturnType<
  typeof discordSDK.commands.authenticate
> extends Promise<infer R>
  ? R & { guildMember: GuildMember | null }
  : never;
//endregion
//region websocket Types
export enum MessageType {
  GAME_MOVEMENT = 0,
  JOIN_GAME = 1,
  GAME_STATE = 2,
  FORFEIT_GAME = 3,
  GAME_OVER = 4,
  GAME_START = 5,
  ROOM_JOIN = 6,
  ROOM_LEAVE = 7,
  ROOM_CREATE = 8,
  ROOM_UPDATE = 9,
}
export enum RoomUpdateType {
  JOINED = 0,
  LEFT = 1,
  MOVED_TO_OBS = 2,
  MOVED_TO_PLAYERS = 3,
}

export type RoomCreateMessage = {
  room_code: string;
  player_id: string;
  public: boolean;
};

export type RoomUpdateChange = {
  type: RoomUpdateType;
  target_id: string;
};

export type RoomJoinMessage = {
  room_code: string;
  player_id: string;
};

export type RoomLeaveMessage = {
  room_code: string;
  player_id: string;
};

export type GameMovementMessage = {
  player_id: string;
  movement: string;
};
export type JoinGameMessage = {
  player_id: string;
  room_code: string;
};
export type GameOverMessage = {
  winner_id: string;
};
export type GameStartMessage = {
  black_player_id: string;
  white_player_id: string;
};

export type Message =
  | { type: MessageType.GAME_MOVEMENT; content: GameMovementMessage }
  | { type: MessageType.JOIN_GAME; content: JoinGameMessage }
  | { type: MessageType.GAME_STATE; content: {} }
  | { type: MessageType.FORFEIT_GAME; content: {} }
  | { type: MessageType.GAME_OVER; content: GameOverMessage }
  | { type: MessageType.GAME_START; content: GameStartMessage }
  | { type: MessageType.ROOM_JOIN; content: RoomJoinMessage }
  | { type: MessageType.ROOM_LEAVE; content: RoomLeaveMessage }
  | { type: MessageType.ROOM_CREATE; content: RoomCreateMessage }
  | {
      type: MessageType.ROOM_UPDATE;
      content: {
        changes: { room_code: string; change: RoomUpdateChange }[];
      };
    };
//endregion
