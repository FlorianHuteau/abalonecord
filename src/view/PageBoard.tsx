import { FC, useEffect } from "react";
import { NodeCell } from "../components/boardComponents/NodeCell";
import { Board } from "../model/Board";
import { Player } from "../model/Player";
import { Node } from "../model/Node";
import useMove from "../hooks/useMove";
import cloneDeep from "lodash/cloneDeep";
interface PageBoardProps {
  currentPlayer: Player;
  setCurrentPlayer: (currentPlayer: Player) => void;
  board: Board;
  setBoard: (board: Board) => void;
}
export const PageBoard: FC<PageBoardProps> = ({
  currentPlayer,
  setCurrentPlayer,
  board,
  setBoard,
}) => {
  const [marblesSelected, directionsAvailablesToMove, handleNode] = useMove(
    board,
    setBoard,
    currentPlayer,
    setCurrentPlayer
  );

  useEffect(() => {
    const initBoard = () => {
      setCurrentPlayer(board.getPlayerByIndex(0));
      const newBoard = cloneDeep(board);
      newBoard.initMatch1v1();
      setBoard(newBoard);
    };

    initBoard();
  }, []);

  const clickNodeHandler = (node: Node) => {
    handleNode(node);
  };

  return (
    <>
      {board.mixBoardAndCellByRow().map((nodes, index) => {
        return (
          <NodeCell
            key={index}
            nodes={nodes}
            rowNumber={index}
            cellByRow={[5, 5, 6, 7, 8, 9, 8, 7, 6, 5, 5]}
            widestRow={Math.max(...board.cellByRow)}
            marblesMoving={marblesSelected}
            clickNodeHandler={clickNodeHandler}
            directionsEligibleToMove={directionsAvailablesToMove}
          />
        );
      })}
    </>
  );
};
