# Abaloncord

Ce projet est une adaptation du jeu Abalone en tant qu'Activity Discord. Plus précisément, il s'agit du front-end sur lequel j'ai travaillé. Mes tâches ont porté sur la création des éléments du jeu (dossier model) et les interactions possibles (hook useMove).

Les technologies utilisées pour le front-end ont été React et Vite.js. Le back-end était en Django, et les applications communiquaient via un webSocket.

Les principales difficultés rencontrées ont été d'implémenter les différentes interactions du jeu avec React, qui est une bibliothèque orientée interface web. La simplicité du jeu a néanmoins facilité le développement.

Ce projet m'a permis d'améliorer mes compétences en travail d'équipe et en gestion de projet. Comme nous étions trois développeurs, nous avons dû nous répartir les tâches, travailler de manière autonome et communiquer efficacement pour résoudre les problèmes le plus rapidement possible.