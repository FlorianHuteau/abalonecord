# Use an official Node.js image as the base
FROM node:22.1.0-alpine3.18

# Set the working directory inside the container
WORKDIR /app

# Copy package.json and package-lock.json to the working directory
COPY discord-app/ ./
RUN rm -rf node_modules package-lock.json
# Install dependencies
RUN yarn install
# Build the app
RUN yarn run build

# Expose the port that the app will run on
EXPOSE 3000

# Define the command to start the app
CMD ["yarn", "run", "start"]